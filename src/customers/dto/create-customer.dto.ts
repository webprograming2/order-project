import { IsNotEmpty, Length, IsPositive } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNotEmpty()
  age: number;

  @Length(10, 10)
  @IsNotEmpty()
  tel: string;

  @Length(1, 1)
  @IsNotEmpty()
  gender: string;
}
