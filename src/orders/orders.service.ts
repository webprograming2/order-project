import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
    @InjectRepository(OrderItem)
    private orderItemRepository: Repository<OrderItem>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    const customer = await this.customerRepository.findOneBy({
      id: createOrderDto.customerId,
    });
    const order: Order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    await this.orderRepository.save(order); // ได้ ID

    // const orderItems: OrderItem[] = await Promise.all(
    //   createOrderDto.orderItems.map(async (odItem) => {
    //     const orderItem = new OrderItem();
    //     orderItem.amount = odItem.amount;
    //     orderItem.product = await this.productRepository.findOneBy({
    //       id: odItem.productId,
    //     });
    //     orderItem.name = orderItem.product.name;
    //     orderItem.price = orderItem.product.price;
    //     orderItem.total = orderItem.price * orderItem.amount;
    //     orderItem.order = order;
    //     return orderItem;
    //   }),
    // );

    for (const odItem of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.amount = odItem.amount;
      orderItem.product = await this.productRepository.findOneBy({
        id: odItem.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.total = orderItem.price * orderItem.amount;
      orderItem.order = order;

      await this.orderItemRepository.save(orderItem);
      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
    }

    await this.orderRepository.save(order);
    return await this.orderRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.orderRepository.find({ relations: ['customer', 'orderItems'] });
  }

  async findOne(id: number) {
    const order = await this.orderRepository.findOne({
      where: { id: id },
      relations: ['customer', 'orderItems'],
    });
    if (!order) {
      throw new NotFoundException();
    }
    return order;
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.orderRepository.findOneBy({ id: id });
    if (!order) {
      throw new NotFoundException();
    }
    return this.orderRepository.softRemove(order);
  }
}
